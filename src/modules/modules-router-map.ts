import { AuthRouterModule } from "./auth/auth-router";

export class ModulesRouterMapper {
    public registerefModules: Array<FeatureModuleRouter> = [
        {
            moduleName: AuthRouterModule,
            parser: 'getRoutesFromModules'
        }
    ]
}


export interface FeatureModuleRouter {
    moduleName: any;
    parser: string;
}