import { Request, Response } from "express";
import * as _ from 'lodash';
import PostService from './service';
import Handlers from '../../api/responses/handlers';

class PostController {

    constructor() {

    }

    getAll(res: Response) {
        PostService
            .getAll()
            .then(_.partial(Handlers.onSuccess, res))
            .catch(_.partial(Handlers.onError, res, 'Erro ao buscar todos os Posts'))

    }

    createPost(req: Request, res: Response) {
        PostService
            .create(req.body)
            .then(_.partial(Handlers.onSuccess, res))
            .catch(_.partial(Handlers.onError, res, 'Erro ao salvar novo post'))
            .catch(_.partial(Handlers.dbErrorHandler, res))
    }

    getById(req: Request, res: Response) {
        const userId = parseInt(req.params.id);
        PostService
            .getById(userId)
            .then(_.partial(Handlers.onSuccess, res))
            .catch(_.partial(Handlers.onError, res, 'Post Não Encontrado'))
    }

    updatePost(req: Request, res: Response) {
        const userId = parseInt(req.params.id);
        PostService
            .update(userId, req.body)
            .then(_.partial(Handlers.onSuccess, res))
            .catch(_.partial(Handlers.onError, res, 'Error ao atualizar post'))
    }

    deletePost(req: Request, res: Response) {
        const userId = parseInt(req.params.id);
        PostService
            .delete(userId)
            .then(_.partial(Handlers.onSuccess, res))
            .catch(_.partial(Handlers.onError, res, 'Error ao excluir post'))
    }
}

export default new PostController();