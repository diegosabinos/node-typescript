'use strict';

const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');
const basename = path.basename(__filename);
const config = require('../config/env/config')();
const env = config.env;
const db: any = {};
const modelRelations = require('../models/relations/relations');

var sequelize = "";
if (config.dbURL) {
  sequelize = new Sequelize(config.dbURL, {
    // gimme postgres, please!
    dialect: 'postgres',
    schema: 'agenda'
  });
} else {
  sequelize = new Sequelize(config.db, config.username, config.password, {
    // gimme postgres, please!
    dialect: 'postgres',
    schema: 'agenda'
  });
}

fs
  .readdirSync(__dirname)
  .filter(file => {
    let extension = '.js'
    if (process.env.NODE_ENV == 'development')
      extension = '.ts'
    return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === `${extension}`);
  })
  .forEach(file => {
    const model = sequelize['import'](path.join(__dirname, file));
    db[model.name] = model;
  });

Object.keys(db).forEach(modelName => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

modelRelations(db);

module.exports = db;
