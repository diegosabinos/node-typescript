module.exports = {
    env: 'production',
    db: 'ts-api',
    dialect: 'postgres',
    username: 'adm_coord',
    password: 'adm_coord',
    host: 'localhots',
    serverPort: 3000,
    pgPort: 5432,
    dbURL: 'postgresql://adm_coord:adm_coord@localhost:5432/ts-api',
    secret: 'S3cr3t'
}