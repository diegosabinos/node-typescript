let extensao: string = 'js';
if (process.env.NODE_ENV == 'development') extensao = 'ts';
module.exports = () => require(`./${process.env.NODE_ENV}.env.${extensao}`)
