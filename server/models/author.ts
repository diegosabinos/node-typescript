export default function (sequelize, DataTypes) {
    const Author = sequelize.define('Author', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                notEmpty: true
            }
        }
    });

    // Author.associate = function (models) {
    //     Author.hasMany(models.Post, { as: 'post' })
    // }
    return Author;
}