import { Request, Response } from "express";
import * as _ from 'lodash';
import AuthorService from './service';
import Handlers from '../../api/responses/handlers';

class AuthorController {

    constructor() {

    }

    getAll(res: Response) {
        AuthorService
            .getAll()
            .then(_.partial(Handlers.onSuccess, res))
            .catch(_.partial(Handlers.onError, res, 'Erro ao buscar todos os autores'))

    }

    createAuthor(req: Request, res: Response) {
        AuthorService
            .create(req.body)
            .then(_.partial(Handlers.onSuccess, res))
            .catch(_.partial(Handlers.onError, res, 'Erro ao salvar novo autor'))
            .catch(_.partial(Handlers.dbErrorHandler, res))
    }

    getById(req: Request, res: Response) {
        const userId = parseInt(req.params.id);
        AuthorService
            .getById(userId)
            .then(_.partial(Handlers.onSuccess, res))
            .catch(_.partial(Handlers.onError, res, 'Autor Não Encontrado'))
    }

    updateAuthor(req: Request, res: Response) {
        const userId = parseInt(req.params.id);
        AuthorService
            .update(userId, req.body)
            .then(_.partial(Handlers.onSuccess, res))
            .catch(_.partial(Handlers.onError, res, 'Error ao atualizar autor'))
    }

    deleteAuthor(req: Request, res: Response) {
        const userId = parseInt(req.params.id);
        AuthorService
            .delete(userId)
            .then(_.partial(Handlers.onSuccess, res))
            .catch(_.partial(Handlers.onError, res, 'Error ao excluir autor'))
    }
}

export default new AuthorController();