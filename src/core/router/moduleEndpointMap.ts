export interface ModuleEndpointMap {
    [key: string]: HTTPVerMap;
}


export interface HTTPVerMap {
    get?: Array<FeatureModuleRouterInfo>,
    post?: Array<FeatureModuleRouterInfo>,
    put?: Array<FeatureModuleRouterInfo>,
    patch?: Array<FeatureModuleRouterInfo>,
    delete?: Array<FeatureModuleRouterInfo>
}

export interface FeatureModuleRouterInfo {
    endpoint: string;
    callback: Function;
    isProtected: boolean;
}