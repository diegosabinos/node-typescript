import { Request, Response } from "express";
import { ModuleEndpointMap } from "./moduleEndpointMap";


export class BaseRouterModule {
    protected readonly context: string = '/api';
    protected version: string = 'v1';
    protected moduleName: string = 'rest-api';

    constructor(moduleName: string) {
        if (typeof moduleName === 'string') {
            this.moduleName = moduleName;
        }
    }

    protected MODULES_ENDPOINT_MAP: ModuleEndpointMap = {
        [this.moduleName]: {
            get: [{
                endpoint: `${this.context}/${this.version}/${this.moduleName}`,
                callback: (req: Request, res: Response) => {
                    res.sendStatus(200).send({ status: 200, msg: 'ok' })
                },
                isProtected: false
            }]
        }
    }

    public getRoutesFromModules(): ModuleEndpointMap {
        return this.MODULES_ENDPOINT_MAP;
    }
}