import { Request, Response } from "express";
import * as _ from 'lodash';
import UserService from './service';
import Handlers from '../../api/responses/handlers';

class UserController {

    constructor() {

    }

    getAll(res: Response) {
        UserService
            .getAll()
            .then(_.partial(Handlers.onSuccess, res))
            .catch(_.partial(Handlers.onError, res, 'Erro ao buscar todos os usuários'))

    }

    createUser(req: Request, res: Response) {
        UserService
            .create(req.body)
            .then(_.partial(Handlers.onSuccess, res))
            .catch(_.partial(Handlers.onError, res, 'Erro ao salvar novo usuário'))
            .catch(_.partial(Handlers.dbErrorHandler, res))
    }

    getById(req: Request, res: Response) {
        const userId = parseInt(req.params.id);
        UserService
            .getById(userId)
            .then(_.partial(Handlers.onSuccess, res))
            .catch(_.partial(Handlers.onError, res, 'Usuário Não Encontrado'))
    }

    updateUser(req: Request, res: Response) {
        const userId = parseInt(req.params.id);
        UserService
            .update(userId, req.body)
            .then(_.partial(Handlers.onSuccess, res))
            .catch(_.partial(Handlers.onError, res, 'Error ao atualizar usuário'))
    }

    deleteUser(req: Request, res: Response) {
        const userId = parseInt(req.params.id);
        UserService
            .delete(userId)
            .then(_.partial(Handlers.onSuccess, res))
            .catch(_.partial(Handlers.onError, res, 'Error ao excluir usuário'))
    }
}

export default new UserController();