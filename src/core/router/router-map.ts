import { ModuleEndpointMap } from "./moduleEndpointMap";

export class RouterModuleFactory {
    private routerModulesMap: Array<ModuleEndpointMap> = [];

    constructor() {
        this.bootstrapModules(new ModulesRouterMapper);
    }

    private bootstrapModules(routerModulesMapper: ModulesRouterMapper) {
        this.routerModulesMap = routerModulesMapper.registerModules.map(this.createModules.bind(this));
    }

    private createModules(registerModules: FeatureModuleRouter): Array<ModuleEndpointMap> {
        const { moduleName, parser } = registerModules;
        return new moduleName()[parser]();
    }

    public getResgisterdModule() {
        return this.routerModulesMap.map((routerModule: ModuleEndpointMap) => {
            const moduleName: string = Object.keys(routerModule)[0];
            return routerModule[moduleName];
        })
    }
}