import { Request, Response, ErrorRequestHandler, NextFunction } from "express";
import * as HTTPStatus from 'http-status';
import * as jwt from 'jwt-simple';
import * as bcrypt from 'bcrypt';
const config = require('../../config/env/config')();

class Handlers {

    authSuccess(res: Response, credentials: any, data: any) {
        const isMatch = bcrypt.compareSync(credentials.password, data.password);

        if (isMatch) {
            const payload = { id: data.id };
            res.json({
                token: jwt.encode(payload, config.secret)
            });
        } else {
            res.status(HTTPStatus.UNAUTHORIZED);
        }
    }

    authFail(res: Response) {
        res.status(HTTPStatus.UNAUTHORIZED);
    }

    onSuccess(res: Response, data: any) {
        res.status(HTTPStatus.OK).json({
            payload: data
        })
    }

    onError(res: Response, error: any, message: string) {
        console.log(`ERROR: ${error}`);
        res.status(HTTPStatus.INTERNAL_SERVER_ERROR).json({ message })
    }

    errorHandlerApi(err: ErrorRequestHandler, _req: Request, res: Response, _next: NextFunction) {
        console.error(`API error handler foi executada: ${err}`)
        res.status(HTTPStatus.INTERNAL_SERVER_ERROR).json({
            erroCode: 'ERR-001',
            message: 'Error Interno do Servidor1'
        });
    }

    dbErrorHandler(res: Response, error: any) {
        console.log(`ERROR: ${error}`);
        res.status(HTTPStatus.INTERNAL_SERVER_ERROR).json({
            code: 'ERR-BD',
            message: 'Error no Banco de Dados'
        })
    }
}

export default new Handlers();