import express from 'express';
import { Application } from 'express';
import morgan from "morgan";
import * as bodyParser from "body-parser";
import Routes from './routes/routes';
import Handlers from './responses/handlers';
import Auth from '../auth';


class Api {

    public express: Application;

    constructor() {
        this.express = express();
        this.middleware();
    }

    middleware() {
        this.express.use(morgan('dev'));
        this.express.use(bodyParser.urlencoded({ extended: true }));
        this.express.use(bodyParser.json());
        this.express.use(Handlers.errorHandlerApi);
        this.express.use(Auth.config().initialize());
        this.routes(this.express, Auth);
    }

    private routes(app: Application, auth: any): void {
        Routes.initRoutes(app, auth);
    }

}

export default new Api().express;