import { Application } from 'express';
import { RouterModuleFactory } from './router-map';
import { HTTPVerMap, FeatureModuleRouterInfo } from './moduleEndpointMap';

export class RouterModule {

    private routerFactory: RouterModuleFactory;
    private express: Application;

    constructor(app: Application) {
        this.express = app;
        this.routerFactory = new RouterModuleFactory();
    }

    public exposeRoutes(authenticate?: Function): void {
        const registerdModules = this.routerFactory.getResgisterdModule();
        if (registerdModules && Array.isArray(registerdModules)) {
            registerdModules.forEach(this.extractRouterInfoFromModule.bind(this, authenticate));
        }
    }

    private extractRouterInfoFromModule(authenticate: Function, routerFearModule: HTTPVerMap) {
        if (routerFearModule) {
            const registerdVerbs = Object.keys(routerFearModule);
            registerdVerbs.forEach(this.extractInfoByVerb.bind(this, authenticate, routerFearModule));
        }
    }

    private extractInfoByVerb(authenticate: Function, routerFearModule: HTTPVerMap, registerdVerb: string) {
        routerFearModule[registerdVerb].forEach(this.mountRoutes.bind(this, authenticate, registerdVerb));
    }

    private mountRoutes(authenticate: Function, registeredVerb: string, routerInfo: FeatureModuleRouterInfo) {
        if (routerInfo) {
            const { isProtected, callback, endpoint } = routerInfo;
            isProtected
                ? this.express.route(endpoint).all(authenticate())[registeredVerb](callback)
                : this.express.route(endpoint)[registeredVerb](callback)
        }
    }
}